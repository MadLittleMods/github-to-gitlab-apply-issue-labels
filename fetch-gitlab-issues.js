const readline = require('readline');
const getCursorPosition = require('get-cursor-position');
const Promise = require('bluebird');
const fetch = require('node-fetch');
const fs = require('fs-extra');
const outputFile = Promise.promisify(fs.outputFile);
const pathExists = Promise.promisify(fs.pathExists);
const throttle = require('lodash.throttle');
const splitArray = require('./lib/split-array');
const logUtility = require('./lib/log-utility');

let config = {};
try {
	config = require('./config.json');
} catch(err) {
	logUtility.log('No config file detected or some error occured when parsing');
}

const opts = Object.assign({}, config,
	require('yargs')
		.option('gitlab-access-token', {
			required: false,
			description: 'Access token used to hit the GitLab API (can also be provided in config.json under the `gitlabAccessToken` key)',
		})
		.option('project-id', {
			required: true,
			description: 'GitLab project to fetch issues from',
		})
		.option('start-page', {
			required: false,
			description: 'Where to start paginating from',
			default: 1,
		})
		.option('stop-iid', {
			required: false,
			description: 'Where to stop paginating',
			type: Number,
			default: 1,
		})
		.option('force-overwrite', {
			alias: 'f',
			required: false,
			description: 'Force overwrite over existing local copy of an issue',
			default: 3,
		})
		.option('concurrency', {
			alias: 'c',
			required: false,
			description: 'Number of issue pages to fetch at once',
			default: 3,
		})
		.help('help')
		.alias('help', 'h')
		.argv
);



function fetchProject(projectId) {
	return Promise.resolve(fetch(`https://gitlab.com/api/v4/projects/${projectId}`, {
			headers: {
				'PRIVATE-TOKEN': opts.gitlabAccessToken
			}
		}))
		.then((res) => {
			return res.json();
		});
}

function fetchIssues(projectId, page = 1) {
	const url = `https://gitlab.com/api/v4/projects/${projectId}/issues?per_page=100&page=${page}`;
	return Promise.resolve(fetch(url, {
			headers: {
				'PRIVATE-TOKEN': opts.gitlabAccessToken
			}
		}))
		.then((res) => {
			return res.json();
		});
}


function paginateUntilDate({
	fetchFunc,
	startPage = 1,
	stopIid = 1,
	concurrency = 1
}) {
	const requests = Array.apply(null, { length: concurrency }).map((o, index) => {
		return fetchFunc(startPage + index);
	});

	return Promise.all(requests)
		.then(function(responses) {
			const list = responses.reduce((prevList, items) => {
				return prevList.concat(items);
			}, []);

			// Higher to Lower, descending
			const lastIssue = list[list.length - 1];
			const isPastStopIid = !lastIssue || (lastIssue.iid < stopIid);
			logUtility.log(`Page: ${startPage} + ${concurrency}, Issue: ${lastIssue && lastIssue.iid} (${lastIssue && lastIssue.created_at}), List: ${list.length}`);
			if(!isPastStopIid) {
				return paginateUntilDate({
					fetchFunc,
					startPage: startPage + concurrency,
					stopIid,
					concurrency
				})
					.then((nextList) => {
						return nextList.concat(list);
					});
			}

			if(isPastStopIid) {
				logUtility.log('list', list);
			}

			return list;
		});
}


if(!opts.gitlabAccessToken) {
	throw new Error('GitLab access token is required');
}

const chunkStatusMap = {
	/* * /
	0: {
		cursor: 0,
		cursorInfo: {
			iid: 123,
			createdAt: '2017-12-07T21:24:33.431Z'
		},
		total: 100,
	}
	/* */
};

function getChunkStatusMessage(chunkStatusMap) {
	return Object.keys(chunkStatusMap).reduce((resultantString, chunkKey) => {
		const chunkStatus = chunkStatusMap[chunkKey];
		const chunkMessage = `Chunk ${chunkKey}: ${ (chunkStatus.cursor >= (chunkStatus.total - 1)) ? 'Done      ' : 'Working...' } currently on ${chunkStatus.cursor + 1}/${chunkStatus.total}, ${chunkStatus.cursorInfo.iid}, ${chunkStatus.cursorInfo.createdAt}`;
		return `${resultantString}${chunkMessage}\n`;
	}, '');
}

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

let updatingCliAreaPosition = {
	x: 0,
	y: 0
};

function updateCli() {
  readline.cursorTo(rl, updatingCliAreaPosition.x, updatingCliAreaPosition.y);
	readline.clearScreenDown(rl);
	rl.write(getChunkStatusMessage(chunkStatusMap));
}

const throttledUpdateCli = throttle(updateCli, 500);


logUtility.log('Fetching Issues...');
Promise.props({
	project: fetchProject(opts.projectId),
	issues: paginateUntilDate({
		fetchFunc: fetchIssues.bind(null, opts.projectId),
		startPage: opts.startPage,
		stopIid: opts.stopIid,
		concurrency: opts.concurrency,
	})
})
	.then(function({ project, issues }) {
		logUtility.log(`Project: ${project.full_name}`);
		logUtility.log(`Found ${issues.length} issues`);
		logUtility.log('--------------------------------------------');

		// Mark where we can start overwriting the console
		const cursorPosition = getCursorPosition.sync();
		updatingCliAreaPosition = {
			x: 0,
			y: cursorPosition.row,
		};

		const fetchLogChunkPromises = splitArray(issues, opts.concurrency).map((chunk, chunkIndex) => {
			return Promise.each(chunk, (issue, index) => {
				const logPath = `./gitlab-issues/${issue.iid}.json`;
				return pathExists(logPath)
					.timeout(5000)
					.then((exists) => {
						// Log our current progress
						if(index === 0 || (index + 1) % 5 === 0 || (chunk.length - index < 5)) {
							chunkStatusMap[chunkIndex] = Object.assign({}, (chunkStatusMap[chunkIndex] || {}), {
								cursor: index,
								cursorInfo: Object.assign({}, (chunkStatusMap[chunkIndex] || {}).cursorInfo || {}, {
									iid: issue.iid,
									createdAt: issue.created_at
								}),
								total: chunk.length,
							});
							throttledUpdateCli();
						}

						if(opts.forceOverwrite || !exists) {
							return outputFile(logPath, JSON.stringify(issue, null, 2))
								.catch((err) => {
									logUtility.log('Problem persisting issue', err, err.stack);
									process.exit(1);
								});
						}

						return Promise.resolve();
					});
			});
		});

		return Promise.all(fetchLogChunkPromises);
	})
	.then(() => {
		logUtility.log('Finished fetching issues!');
	})
	.catch((err) => {
		logUtility.log('err', err);
	})
	.then(() => {
		rl.close();
	});
