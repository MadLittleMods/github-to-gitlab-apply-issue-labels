const Promise = require('bluebird');
const readline = require('readline');
const getCursorPosition = require('get-cursor-position');
const path = require('path');
const fs = require('fs-extra');
const readFile = Promise.promisify(fs.readFile);
const readdir = Promise.promisify(fs.readdir);
const fetch = require('node-fetch');
const throttle = require('lodash.throttle');
const logUtility = require('./lib/log-utility');
const splitArray = require('./lib/split-array');


const githubIssuesDir = './github-issues/';
const gitlabIssuesDir = './gitlab-issues/';


let config = {};
try {
	config = require('./config.json');
} catch(err) {
	logUtility.log('No config file detected or some error occured when parsing');
}

const opts = Object.assign({}, config,
	require('yargs')
		.option('ascending', {
			type: 'boolean',
			required: false,
			description: 'Whether to start processing the newest files first',
			default: false,
		})
		.option('start-iid', {
			required: false,
			description: 'Where to start',
			alias: 'start-id',
			type: Number,
			default: 1,
		})
		.option('stop-iid', {
			required: false,
			description: 'Where to stop',
			alias: 'stop-id',
			type: Number,
			default: Infinity,
		})
		.option('concurrency', {
			alias: 'c',
			required: false,
			description: 'Number of files to read at once',
			default: 3,
		})
		.help('help')
		.alias('help', 'h')
		.argv
);


if(!opts.gitlabAccessToken) {
	throw new Error('GitLab access token is required');
}

if(opts.startIid > opts.stopIid) {
	throw new Error('Start IID can\'t be larger than the stop IID');
}
else if(opts.stopIid < opts.startIid) {
	throw new Error('Stop IID can\'t be smaller than the start IID');
}



function fetchProject(projectId) {
	const url = `https://gitlab.com/api/v4/projects/${projectId}`;
	return Promise.resolve(fetch(url, {
			headers: {
				'PRIVATE-TOKEN': opts.gitlabAccessToken
			}
		}))
		.then((res) => {
			if(res.status !== 200) {
				throw new Error(`${res.status} ${res.statusText}, ${url}`);
			}

			return res.json();
		});
}


function applyLabels(projectId, iid, labels) {
	const url = `https://gitlab.com/api/v4/projects/${projectId}/issues/${iid}?labels=${labels.join(',')}`;
	return Promise.resolve(fetch(url, {
			method: 'PUT',
			headers: {
				'PRIVATE-TOKEN': opts.gitlabAccessToken
			}
		}))
		.then((res) => {
			if(res.status !== 200) {
				throw new Error(`${res.status} ${res.statusText}, ${url}`);
			}

			return res.json();
		});
}


const chunkStatusMap = {
	/* * /
	0: {
		cursor: 0,
		cursorInfo: {
			iid: 123,
			createdAt: '2017-12-07T21:24:33.431Z'
		},
		total: 100,
	}
	/* */
};

function getChunkStatusMessage(chunkStatusMap) {
	return Object.keys(chunkStatusMap).reduce((resultantString, chunkKey) => {
		const chunkStatus = chunkStatusMap[chunkKey];
		const chunkMessage = `Chunk ${chunkKey}: ${ (chunkStatus.cursor >= (chunkStatus.total - 1)) ? 'Done      ' : 'Working...' } currently on ${chunkStatus.cursor + 1}/${chunkStatus.total}, ${chunkStatus.cursorInfo.iid}`;
		return `${resultantString}${chunkMessage}\n`;
	}, '');
}

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

let updatingCliAreaPosition = {
	x: 0,
	y: 0
};

function updateCli() {
  readline.cursorTo(rl, updatingCliAreaPosition.x, updatingCliAreaPosition.y);
	readline.clearScreenDown(rl);
	rl.write(getChunkStatusMessage(chunkStatusMap));
}

const throttledUpdateCli = throttle(updateCli, 500);



let issuesProcessedCount = 0;
const problemIssues = [];

Promise.props({
	project: fetchProject(opts.projectId),
	githubIssueFiles: readdir(githubIssuesDir),
	gitlabIssueFiles: readdir(gitlabIssuesDir)
})
	.then(({ project, githubIssueFiles, gitlabIssueFiles }) => {
		logUtility.log(`Project: ${project.full_name}`);
		console.log(`Available files -> github:${githubIssueFiles.length} gitlab:${gitlabIssueFiles.length}`);
		logUtility.log('--------------------------------------------');

		// Mark where we can start overwriting the console
		const cursorPosition = getCursorPosition.sync();
		updatingCliAreaPosition = {
			x: 0,
			y: cursorPosition.row,
		};

		const applyLabelChunkPromises = splitArray(githubIssueFiles, opts.concurrency).map((chunk, chunkIndex) => {
			return chunk.reduce((promiseChain, issueFileName, index) => {
				return promiseChain.then(() => {
					const iid = parseInt(issueFileName, 10);
					// Skip anything outside of our given range
					if(iid < opts.startIid || iid > opts.stopIid) {
						return Promise.resolve();
					}

					// Log our current progress
					if(iid === 0 || (iid + 1) % 5 === 0 || (opts.stopIid - iid < 5)) {
						chunkStatusMap[chunkIndex] = Object.assign({}, (chunkStatusMap[chunkIndex] || {}), {
							cursor: index,
							cursorInfo: Object.assign({}, (chunkStatusMap[chunkIndex] || {}).cursorInfo || {}, {
								iid: iid,
							}),
							total: chunk.length,
						});
						throttledUpdateCli();
					}

					// Start the processing
					return Promise.props({
						githubFileText: readFile(path.join(githubIssuesDir, issueFileName), 'utf8'),
						gitlabFileText: readFile(path.join(gitlabIssuesDir, issueFileName), 'utf8')
					})
						.then(({ githubFileText, gitlabFileText }) => ({
							githubIssue: JSON.parse(githubFileText),
							gitlabIssue: JSON.parse(gitlabFileText)
						}))
						.then(({ githubIssue, gitlabIssue }) => {
							//console.log('githubIssue', githubIssue);
							//console.log('gitlabIssue', gitlabIssue);

							const githubLabels = (githubIssue.labels || []).map((label) => {
								return label.name;
							});
							const gitlabLabels = gitlabIssue.labels;

							// We combine both labels so we don't lose any we might of added in the mean time
							const labelsToApply = githubLabels.concat(gitlabLabels);

							//console.log('labelsToApply', labelsToApply);
							return applyLabels(opts.projectId, iid, labelsToApply)
								.then(() => {
									issuesProcessedCount = issuesProcessedCount + 1;
								});
						})
						.catch((err) => {
							problemIssues.push(iid);

							console.log(`Problem processing ${iid}`, err);
						});
				});
			}, Promise.resolve());
		});

		return Promise.all(applyLabelChunkPromises);
	})
	.then(() => {
		logUtility.log(`Finished applying labels! count:${issuesProcessedCount}`);
		logUtility.log(`Problem issues! count:${problemIssues.length}`, problemIssues);
	})
	.catch((err) => {
		logUtility.log('err', err);
	});
