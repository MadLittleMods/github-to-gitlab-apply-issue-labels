# Apply labels from GitHub issues to GitLab issues

Quick and dirty script to fetch GitHub/GitLab issues. Then apply the labels from GitHub to the GitLab project

## Setup

Get a [GitLab personal access token](https://docs.gitlab.com/ee/api/README.html#personal-access-tokens) and save it to `config.json` or pass it in as a command line argument, `--gitlab-access-token`, `--github-access-token`.

`config.json`
```json
{
	"gitlabAccessToken": "foobarbaz",
	"githubAccessToken": "foobarbaz"
}
```


## Scripts


### `fetch-github-issues.js`

Paginate from the starting page to the stop date and grab all of the jobs during that time. Fetch if necessary and save to `./github-issues/:id.txt`

Currently only looks through `failed` jobs.

```
node fetch-github-issues.js --project-url gitterHQ/gitter --concurrency=3
```


### `fetch-gitlab-issues.js`

Paginate from the starting page to the stop date and grab all of the jobs during that time. Fetch if necessary and save to `./gitlab-issues/:id.txt`

```
node fetch-gitlab-issues.js --project-id gitlab-org%2Fgitter%2Fwebapp --concurrency=3
```


### `apply-labels-to-gitlab.js`

Go through all of the files in `./jobs/` and grep for the given value. Or just use your favorite grep flavor on the `./jobs/*` files.

```
node apply-labels-to-gitlab.js --project-id gitlab-org%2Fgitter%2Fwebapp
node apply-labels-to-gitlab.js --project-id gitlab-org%2Fgitter%2Fwebapp --start-id=1733 --stop-id=1733
```
