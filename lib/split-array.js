// Splits an array (not sequential)
// splitArray([1,2,3,4,5,6,7,8,9,10,11,12], 3)
// -> [[1, 4, 7, 10], [2, 5, 8, 11], [3, 6, 9, 12]]
module.exports = function splitArray(arr, numberOfPieces) {
  return arr.reduce((chunks, item, index) => {
    chunks[index % numberOfPieces] = chunks[index % numberOfPieces] || [];
    chunks[index % numberOfPieces].push(item);
    return chunks;
  }, []);
};
